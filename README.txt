Provides per node (and later per entity) google analytics reports. The reports
uses views and fields provided by Google Analytics Reports Module displaying
them in the page and layout provided by Hidden Tab Module.
